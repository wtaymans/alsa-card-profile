A wrapper around the PulseAudio ALSA card management code.

This is a little library that probes an alsa card an contructs
a card model with profiles, ports and devices. It is then also possible
to listen to events such as volume changes and jack events.

To build:

```
meson build
ninja -C build
```

There is a test application in src that can be run from the build
directory like this:

```
build/src/acp-tool -c <card-index>
```

This will drop you in an interactive shell. Use the 'help' command
for more instructions.

To install use:

```
meson -C build install
```

There is a library included called libacp. Look at the header file acp.h
for more information.
